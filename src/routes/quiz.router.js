const express = require('express');
const router = express.Router();
const QuizService = require('../services/quiz.service');

const authMiddleware = require('../middleware/auth');

router.post('/', authMiddleware, async (req, res, next) => {
  const result = await QuizService.checkQuestion(req.body);

  res.json(result);
  res.status(result.status);
});

module.exports = router;
