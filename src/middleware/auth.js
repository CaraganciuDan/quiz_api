const admin = require('firebase-admin');

module.exports = (req, res, next) => {
  const token = req.headers.authorization;
  if (token) {
    admin.auth().verifyIdToken(token)
      .then(() => {
        next()
      }).catch((e) => {
      res.status(403).send('Unauthorized')
    });
  } else {
    res.status(403).send('Unauthorized')
  }
};
